import { createAsyncThunk } from "@reduxjs/toolkit";
import * as api from "../Api";

export const getNews = createAsyncThunk("app/getNews", async (thunkAPI) => {
  try {
    const {
      data: { articles },
    } = await api.getNewsApi();
    return articles;
  } catch (error) {
    return thunkAPI.rejectWithValue(error.response.data);
  }
});
