import { createSlice } from "@reduxjs/toolkit";
import { getNews } from "./News.async";

const initialState = {
  isAppBarOpen: true,
  drawerWidth: 250,
  loading: true,
};

let appSlice = createSlice({
  name: "users",
  initialState,
  reducers: {
    toggleAppBar(state) {
      let appStatus = state.isAppBarOpen;
      state.isAppBarOpen = !appStatus;
    },
  },
  extraReducers: {
    [getNews.pending]: (state, action) => {
      state.loading = true;
    },
    [getNews.fulfilled]: (state, action) => {
      state.loading = false;
      state.articles = action.payload;
    },
    [getNews.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },
  },
});

export default appSlice.reducer;

export const { toggleAppBar } = appSlice.actions;
