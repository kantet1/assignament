import axios from "axios";

const URL = `https://newsapi.org/v2/top-headlines?country=us&apiKey=${process.env.REACT_APP_API_KEY}`;

export const getNewsApi = () => axios.get(URL);

//1f7782a4064f4afe81a1c4d47b7f0226
