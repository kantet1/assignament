import { configureStore } from "@reduxjs/toolkit";
import appReducer from "./slice/app.states.slice";

export const store = configureStore({
  reducer: {
    app: appReducer,
  },
});
