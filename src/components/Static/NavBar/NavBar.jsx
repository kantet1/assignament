import React from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  IconButton,
  Toolbar,
  Typography,
  Box,
  Badge,
  Container,
} from "@mui/material";
import ElectricBoltOutlinedIcon from "@mui/icons-material/ElectricBoltOutlined";
import MenuIcon from "@mui/icons-material/Menu";

import helper from "../../../constants";
import { toggleAppBar } from "../../../redux/slice/app.states.slice";
import { styles } from "./style";

function NavBar({ active }) {
  const { isAppBarOpen } = useSelector((state) => state?.app);

  const dispatch = useDispatch();
  const handleDrawerToggle = () => {
    dispatch(toggleAppBar());
  };

  return (
    <helper.AppBar position="fixed" elevation={0} open={isAppBarOpen}>
      <Toolbar
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerToggle}
            edge="start"
            sx={{
              marginRight: 5,
              ...(isAppBarOpen && { display: "none" }),
            }}
          >
            <MenuIcon />
          </IconButton>
        </Box>
        <Container
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Typography variant="h6" sx={styles.head} color="text.secondary">
            {active}
          </Typography>
          <Box sx={{ display: "flex", alignItems: "center" }}>
            <ElectricBoltOutlinedIcon />
            <Badge badgeContent={4} color="error">
              <Typography color="text.secondary">What's new</Typography>
            </Badge>
          </Box>
        </Container>
      </Toolbar>
    </helper.AppBar>
  );
}

export default NavBar;
