import React from "react";
import { ListItemButton, ListItemIcon, ListItemText, Box } from "@mui/material";
import { useSelector } from "react-redux";
import { blue } from "@mui/material/colors";

import { styles } from "./SideStyles";

function SideNavLinks(props) {
  const { isAppBarOpen } = useSelector((state) => state?.app);
  const isselected = props?.active === props?.text ? true : false;
  console.log(isselected);
  return (
    <ListItemButton
      key={props?.text}
      selected={isselected}
      sx={styles.navLinksContainer}
      style={{
        borderLeft: `2px solid ${isselected ? blue[500] : "white"}`,
      }}
    >
      <ListItemIcon sx={styles.navIcon}>
        <props.Icon sx={{ color: isselected ? blue[500] : "#333333" }} />
      </ListItemIcon>
      <ListItemText
        primary={props?.text}
        sx={{
          opacity: isAppBarOpen ? 1 : 0,
          m: 0,
          color: isselected ? blue[500] : "#333333",
          fontSize: "14px",
          fontWeight: "400",
          lineHeight: "16px",
        }}
      />
      {props?.fab && (
        <Box component="div" sx={styles.fab}>
          {props?.fab}
        </Box>
      )}
    </ListItemButton>
  );
}

export default SideNavLinks;
