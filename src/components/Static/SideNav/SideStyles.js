import { store } from "../../../redux/store";
import { purple, orange, grey, blue, green } from "@mui/material/colors";

const { isAppBarOpen } = store.getState().app;

export const styles = {
  navHeadContainer: {
    width: "100%",
    display: "flex",
    justifyContent: "flex-start",
    ml: 2,
  },
  textBold: {
    fontWeight: "bold",
    fontSize: "20px",
  },
  fab: {
    height: "20px",
    width: "20px",
    borderRadius: "50%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: green[400],
    color: green[50],
    fontSize: "14px",
  },

  navLinksContainer: {
    paddingTop: 0,
    paddingBottom: 0,
    textDecoration: "none",
    borderLeft: "1px solid #fff",
  },
  navLinkIconContainer: {
    minWidth: 0,
    mr: isAppBarOpen ? 3 : "auto",
    justifyContent: "center",
  },
  navIcon: {
    minWidth: 0,
    mr: isAppBarOpen ? 3 : "auto",
    justifyContent: "center",
    mr: 2,
  },
};
