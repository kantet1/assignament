import React from "react";
import { Box, List } from "@mui/material";
import { Link } from "react-router-dom";
import SideNavLinks from "./SideNavLinks";
import Collapsible from "./Collapsible";

import { styles } from "./SideStyles";

function SideNavLinkContainer(props) {
  return (
    <List sx={styles.navList}>
      {props?.suLinks ? (
        <Collapsible {...props} />
      ) : (
        <Link style={{ textDecoration: "none" }} to={`/${props?.to}`}>
          <SideNavLinks {...props} />
        </Link>
      )}
    </List>
  );
}

export default SideNavLinkContainer;
