import React from "react";
import { Box, IconButton, Typography, Divider } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";

import MenuIcon from "@mui/icons-material/Menu";

import helper from "../../../constants";
import { toggleAppBar } from "../../../redux/slice/app.states.slice";
import SideNavLinkContainer from "./SideNavLinkContainer";
import { styles } from "./SideStyles";
import { Selector } from "../../Reusable";

function SideNav({ active }) {
  const { isAppBarOpen } = useSelector((state) => state?.app);
  const dispatch = useDispatch();
  const handleDrawerToggle = () => {
    dispatch(toggleAppBar());
  };

  return (
    <helper.Drawer
      className="app__sidenav"
      variant="permanent"
      open={isAppBarOpen}
    >
      <helper.DrawerHeader>
        <Box sx={styles.navHeadContainer}>
          <Typography color="text.primary" variant="h6" sx={styles.textBold}>
            Vetrina
          </Typography>
          <Typography sx={styles.textBold} color="text.secondary" variant="h6">
            live
          </Typography>
        </Box>
        <IconButton onClick={handleDrawerToggle}>
          <MenuIcon />
        </IconButton>
      </helper.DrawerHeader>

      {helper?.sidenavLinks?.map((data, i) => (
        <SideNavLinkContainer active={active} {...data} key={i} />
      ))}

      <Box sx={{ my: 2 }}>
        <Divider sx={{ mb: 2 }} />
        {helper?.others?.map((data, i) => (
          <SideNavLinkContainer active={active} {...data} key={i} />
        ))}
      </Box>
      {isAppBarOpen && (
        <Box sx={{ pl: 2.5, mb: 10 }}>
          <Typography>Select your shop</Typography>
          <Selector />
        </Box>
      )}
    </helper.Drawer>
  );
}

export default SideNav;
