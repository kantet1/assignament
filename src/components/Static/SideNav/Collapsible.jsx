import {
  Collapse,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import { blue } from "@mui/material/colors";

import { useSelector } from "react-redux";

function Collapsible(props) {
  const [open, setOpen] = useState(false);
  const { isAppBarOpen } = useSelector((state) => state?.app);
  const isselected = props?.active === props?.text ? true : false;
  useEffect(() => {
    if (isAppBarOpen) return;
    setOpen(false);
  }, [isAppBarOpen]);

  return (
    <>
      <ListItemButton
        style={{
          borderLeft: `2px solid ${isselected ? blue[500] : "white"}`,
        }}
        sx={{ my: -1 }}
        onClick={() => setOpen(!open)}
      >
        <ListItemIcon>
          <props.Icon sx={{ color: isselected ? blue[500] : "#333333" }} />
        </ListItemIcon>
        <ListItemText
          primary={props?.text}
          color="text.primary"
          sx={{
            opacity: isAppBarOpen ? 1 : 0,
            ml: -2,
            p: 0,
            color: isselected ? blue[500] : "#333333",
            fontSize: "14px",
            fontWeight: "400",
            lineHeight: "16px",
          }}
        />
        {open ? (
          <ExpandLess sx={{ opacity: isAppBarOpen ? 1 : 0 }} />
        ) : (
          <ExpandMore sx={{ opacity: isAppBarOpen ? 1 : 0 }} />
        )}
      </ListItemButton>

      <Collapse
        in={open}
        sx={{ backgroundColor: blue[50] }}
        timeout="auto"
        unmountOnExit
      >
        <List component="div" disablePadding>
          {props?.suLinks?.map(({ text, to }) => (
            <ListItemButton sx={{ pl: 7, m: 0 }}>
              <ListItemText
                sx={{ p: 0, m: 0, color: "#333333" }}
                primary={text}
              />
            </ListItemButton>
          ))}
        </List>
      </Collapse>
    </>
  );
}

export default Collapsible;
