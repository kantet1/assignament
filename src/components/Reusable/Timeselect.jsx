import { Autocomplete, TextField } from "@mui/material";
import React from "react";

function Timeselect() {
  const defaultProps = {
    options: top100Films,
    getOptionLabel: (option) => option.title,
  };
  return (
    <Autocomplete
      {...defaultProps}
      sx={{ width: 145 }}
      renderInput={(params) => (
        <TextField
          {...params}
          variant="standard"
          sx={{
            fontStyle: "normal",
            fontWeight: "400",
            fontSize: "14px",
            lineHeight: "18px",
          }}
          InputProps={{ ...params.InputProps, disableUnderline: true }}
        />
      )}
    />
  );
}

export default Timeselect;

const top100Films = [{ title: "This month" }, { title: "Last month" }];
