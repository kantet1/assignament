import React from "react";
import { FormControl, InputLabel, Select, MenuItem } from "@mui/material";

function Selector() {
  return (
    <FormControl sx={{ mt: 3, pr: 2 }} fullWidth>
      <InputLabel id="demo-simple-select-label">Store</InputLabel>
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value="Store xp"
        label="Store 1"
      >
        <MenuItem value={1}>Store 1</MenuItem>
        <MenuItem value={2}>Store 2</MenuItem>
        <MenuItem value={3}>Store 3</MenuItem>
      </Select>
    </FormControl>
  );
}

export default Selector;
