import React from "react";
import { Card, Grid } from "@mui/material";
import { styles } from "./style";

function CustomCard({ children, sm, lg, h, color }) {
  return (
    <Grid
      item
      xs={12}
      sm={!sm ? 6 : sm}
      lg={!lg ? 6 : lg}
      sx={{
        px: {
          xs: 0,
          sm: 1,
        },
        py: 1,
        height: h ? h : "auto",
        width: "100%",
      }}
    >
      <Card elevation={2} style={{ backgroundColor: color }} sx={styles?.card}>
        {children}
      </Card>
    </Grid>
  );
}

export default CustomCard;
