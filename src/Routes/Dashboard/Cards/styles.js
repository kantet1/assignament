import { blue, green, orange } from "@mui/material/colors";
export const styles = {
  configure: {
    headtext: {
      fontStyle: "normal",
      fontWeight: "500",
      fontSize: "22px",
      lineHeight: "27px",
    },
    infoText: {
      fontWeight: "600",
      fontSize: "34px",
      lineHeight: "39px",
      color: orange[800],
    },
    infoPar: {
      fontStyle: "normal",
      fontWeight: "400",
      fontSize: "17px",
      lineHeight: "22px",
      mb: 2,
    },
  },

  support: {
    info: { pt: 2, pb: 1, display: "flex", alignItems: "center" },
    img: {
      height: 30,
      width: 30,
      mr: 1,
      objectFit: "cover",
      borderRadius: "50%",
    },
  },
  invite: {
    header: {
      display: "flex",
      fontStyle: "normal",
      fontWeight: "500",
      fontSize: "22px",
      lineHeight: "27px",
    },
    info: {
      fontStyle: "normal",
      fontWeight: "400",
      fontSize: "17px",
      lineHeight: "22px",
      my: 2,
    },
  },
  trustPilot: {
    head: {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-start",
      color: "#ffff",
      fontSize: "20px",
      mb: 1,
    },
    info: {
      color: "white",
      mt: { xs: 0, sm: 4 },
      mb: { xs: 1, sm: 3 },

      fontStyle: "normal",
      fontWeight: "400",
      fontSize: "17px",
      lineHeight: "22px",
    },
    link: {
      color: green[500],
      textDecoration: "underline",
      display: "flex",
      alignItems: "center",
      cursor: "pointer",
    },
  },

  news: {
    topContainer: {
      width: "100%",
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
    },
    header: {
      display: "flex",
      alignItems: "center",
      fontStyle: "normal",
      fontWeight: "500",
      fontSize: "22px",
      lineHeight: "27px",
    },
    anchorTag: {
      textDecoration: "none",
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
      gap: 1.5,
    },
    time: {
      textDecoration: "underline",
      mt: 1,
      fontWeight: "300",
      fontSize: "12px",
      lineHeight: "12px",
    },
    img: {
      height: 100,
      width: 100,
      objectFit: "cover",
    },
    author: {
      mb: 1,
      overflow: "hidden",
      maxWidth: 200,
      maxHeight: "1.1em",
      wordWrap: "break-word",
      fontWeight: "500",
      fontSize: "11px",
      lineHeight: "13px",
    },
    title: {
      overflow: "hidden",
      maxWidth: 300,
      maxHeight: "3.6em",
      // lineHeight: "1.2em",
      wordWrap: "break-word",
      textOverflow: "ellipsis",
      fontWeight: "500",
      fontSize: "15px",
      lineHeight: "18px",
    },
    spinnerContainer: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      height: "50vh",
      width: "100%",
    },
  },
  MarketPlace: {
    headerText: {
      display: "flex",
      alignItems: "center",
      fontStyle: "normal",
      fontWeight: "500",
      fontSize: "20px",
      lineHeight: "25px",
    },
    headerIcon: { mr: 2, fontSize: "25px" },
    info: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      mb: -1,
      width: "100%",
    },
    iconContainer: {
      backgroundColor: green[700],
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      width: "142px",
      height: 130,
      my: 4,
      borderRadius: "10px",
    },
    infoText: {
      mt: -3,

      fontStyle: "normal",
      fontWeight: "400",
      fontSize: "15px",
      lineHeight: "20px",
      width: "152px",
    },
  },

  mobileApp: {
    storeApp: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
    },
    img: {
      height: { xs: 40, md: 50 },
      cursor: "pointer",
    },
    top: {
      display: "flex",
      color: "white",
      flexDirection: { xs: "column", sm: "row" },
    },
    phone: {
      height: { xs: 400, sm: 200, md: 250 },
      mt: -5,
    },
    hugeText: {
      mb: 2,
      fontStyle: "normal",
      fontWeight: "500",
      fontSize: "22px",
      lineHeight: "27px",
    },
  },

  visitor: {
    topContainer: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
    },
  },
  largeText: {
    fontStyle: "normal",
    fontWeight: "500",
    fontSize: "42px",
    lineHeight: "61px",
  },
  info: {
    width: "100%",
    pb: 4,
    mt: 4,
    container: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      my: 1,
    },
    ifnoText: {
      fontStyle: "normal",
      fontWeight: "400",
      fontSize: "17px",
      lineHeight: "22px",
    },
    infoSum: {
      fontStyle: "normal",
      fontWeight: "600",
      fontSize: "17px",
      lineHeight: "22px",
    },
  },
  link: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    textDecoration: "underline",
    cursor: "pointer",
    fontStyle: "normal",
    fontWeight: "400",
    fontSize: "13px",
    lineHeight: "20px",
  },
  container: {
    height: "100%",
    width: "100%",
  },
  headerText: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontWeight: "900",
    fontSize: "20px",
    fontStyle: "normal",
    fontWeight: "500",
    lineHeight: "25px",
  },

  headerIcon: {
    fontSize: "24px",
    mr: 1,
  },
  flex: {
    display: "flex",
    alignItems: "center",
  },
};
