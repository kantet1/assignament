import React from "react";
import { Box, Typography } from "@mui/material";

import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import FormatListBulletedIcon from "@mui/icons-material/FormatListBulleted";

import { CustomCard, CountrySelect } from "../../../components/Reusable";
import { styles } from "./styles";

function Orders() {
  return (
    <CustomCard>
      <Box sx={styles.container}>
        <Box sx={styles?.visitor?.topContainer}>
          <Typography sx={styles.headerText}>
            <FormatListBulletedIcon sx={styles.headerIcon} />
            <span>Orders</span>
          </Typography>
          <CountrySelect />
        </Box>
        <Box sx={styles?.info}>
          {[
            { name: "Order received:", val: "156" },
            { name: "Earnings:", val: "$ 1893,24" },
          ].map(({ name, val }) => (
            <Box sx={styles?.info?.container}>
              <Typography sx={styles?.info?.ifnoText} color="text?.primary">
                {name}
              </Typography>
              <Typography sx={styles?.info?.infoSum}>{val}</Typography>
            </Box>
          ))}
        </Box>
        <Box>
          <Typography sx={styles.link} color="text.secondary">
            <span>10 free tips to increase your sales</span>{" "}
          </Typography>
        </Box>
      </Box>
    </CustomCard>
  );
}

export default Orders;
