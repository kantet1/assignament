import { Box, Grid, Typography } from "@mui/material";
import moment from "moment";
import React from "react";

import { styles } from "./styles";

const random = "https://picsum.photos/300/300";

function NewsCard({ title, urlToImage, author, publishedAt, url }) {
  return (
    <Grid item xs={12}  sm={6}>
      <Box
        sx={styles.news.anchorTag}
        color="text.primary"
        component="a"
        href={url}
      >
        <Box
          sx={styles.news.img}
          component="img"
          src={urlToImage ? urlToImage : random}
        />
        <Box>
          <Typography sx={styles.news.author} color="text.secondary">
            {author ? author : "Lincoln Kantet"}
          </Typography>
          <Typography color="text.primary" sx={styles.news.title}>
            {title}
          </Typography>
          <Typography sx={styles.news.time}>
            {moment(publishedAt).startOf("day").fromNow()}
          </Typography>
        </Box>
      </Box>
    </Grid>
  );
}

export default NewsCard;
