import React, { useEffect } from "react";
import { Typography, Box, Grid, CircularProgress } from "@mui/material";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import InsertDriveFileOutlinedIcon from "@mui/icons-material/InsertDriveFileOutlined";
import moment from "moment";

import { CustomCard } from "../../../components/Reusable";
import { styles } from "./styles";
import { getNews } from "../../../redux/slice/News.async";
import { useDispatch, useSelector } from "react-redux";
import NewsCard from "./NewsCard";

function News() {
  const app = useSelector((state) => state.app);
  const dispatch = useDispatch();
  useEffect(() => {
    getArticles();
  }, []);
  const getArticles = () => {
    if (app.articles?.length > 0) return;
    dispatch(getNews());
  };
  console.log(app.articles);
  return (
    <CustomCard sm={12} lg={12}>
      <Box sx={styles.container}>
        <Box sx={styles.news.topContainer}>
          <Typography sx={styles.news.header}>
            <InsertDriveFileOutlinedIcon />
            Latest news
          </Typography>
          <Typography sx={(styles.flex, styles.link)} color="text.secondary">
            Visit our blog <ArrowForwardIcon sx={{ ml: 2 }} />
          </Typography>
        </Box>

        <Grid container sx={{ py: 3 }} spacing={4}>
          {app?.loading ? (
            <Box sx={styles.news.spinnerContainer}>
              <CircularProgress color="secondary" />
            </Box>
          ) : (
            <>
              {app?.articles &&
                app?.articles?.map((data, i) => (
                  <>{i < 10 && <NewsCard {...data} key={i} />}</>
                ))}
            </>
          )}
        </Grid>
      </Box>
    </CustomCard>
  );
}

export default News;
