import React from "react";
import { Typography, Box } from "@mui/material";
import VisibilityOutlinedIcon from "@mui/icons-material/VisibilityOutlined";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";

import { CountrySelect, CustomCard } from "../../../components/Reusable";
import { styles } from "./styles";

function Visitors() {
  return (
    <CustomCard>
      <Box sx={styles.container}>
        <Box sx={styles?.visitor?.topContainer}>
          <Typography sx={styles.headerText}>
            <VisibilityOutlinedIcon sx={styles.headerIcon} />
            <span>Visitors</span>
          </Typography>

          <CountrySelect />
        </Box>
        <Box sx={styles?.info}>
          <Typography sx={styles?.largeText}>1678</Typography>
        </Box>
        <Box>
          <Typography sx={styles.link} color="text.secondary">
            <span>Do you want more visits? Contact us!</span>{" "}
            <ArrowForwardIcon sx={{ ml: 3.5 }} />
          </Typography>
        </Box>
      </Box>
    </CustomCard>
  );
}

export default Visitors;
