import React from "react";
import { Box, Typography } from "@mui/material";
import GroupOutlinedIcon from "@mui/icons-material/GroupOutlined";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import { green } from "@mui/material/colors";

import { CustomCard } from "../../../components/Reusable";
import { styles } from "./styles";

function Invite() {
  return (
    <CustomCard lg={12}>
      <Box sx={styles?.container}>
        <Typography variant="h5" sx={styles.invite.header}>
          <GroupOutlinedIcon sx={{ fontSize: 30, mr: 1 }} />
          <span>Invite friend</span>
        </Typography>
        <Typography variant="body2" sx={styles.invite.info}>
          <span style={{ color: green[500], fontWeight: "bold" }}>
            Receive 50 products
          </span>
          by inviting a friend who subscribes to a plan. Your friend will
          receive a 30% discount coupon valid for any plan.
        </Typography>
        <Typography color="text.secondary" sx={styles.link}>
          Start inviting friends! <ArrowForwardIcon sx={{ ml: 3.5 }} />
        </Typography>
      </Box>
    </CustomCard>
  );
}

export default Invite;
