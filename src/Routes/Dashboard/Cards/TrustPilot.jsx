import React from "react";
import StarIcon from "@mui/icons-material/Star";
import { green } from "@mui/material/colors";
import { Box, Typography } from "@mui/material";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";

import { styles } from "./styles";
import { CustomCard } from "../../../components/Reusable";

function TrustPilot() {
  return (
    <CustomCard lg={12} color="#000032">
      <Box sx={styles?.container}>
        <Typography sx={styles.trustPilot.head}>
          <StarIcon sx={{ color: green[600], fontSize: "30px" }} />{" "}
          <span>TrustPilot</span>
        </Typography>
        <Typography variant="body2" sx={styles.trustPilot.info}>
          Show us your love by leaving a{" "}
          <span style={{ color: green[500] }}>Positive</span> review on trust
          pilot and receive the extension of
          <span style={{ fontWeight: "bold" }}> 50 additional products</span>
        </Typography>
        <Typography variant="body2" sx={styles.trustPilot.link}>
          <span>Write a review of TrustPilot</span>{" "}
          <ArrowForwardIcon sx={{ ml: 3.5 }} />
        </Typography>
      </Box>
    </CustomCard>
  );
}

export default TrustPilot;
