import React from "react";
import { Box, Typography } from "@mui/material";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";

import { CustomCard } from "../../../components/Reusable";
import { images } from "../../../assets";
import { styles } from "./styles";

function MobileApp() {
  return (
    <CustomCard color="#F3A35C">
      <Box sx={styles?.container}>
        <Box sx={styles?.mobileApp?.top}>
          <Box>
            <Typography variant="h3" sx={styles?.mobileApp?.hugeText}>
              Sell your products on your exclusive App published on the stores
            </Typography>
            <Typography sx={styles?.link}>
              <span> Show more</span> <ArrowForwardIcon sx={{ pl: 1 }} />
            </Typography>
          </Box>
          <Box
            component="img"
            sx={styles?.mobileApp?.phone}
            src={images.phone}
          />
        </Box>
        <Box sx={styles?.mobileApp?.storeApp}>
          {[images?.google, images?.apple].map((img) => (
            <Box component="img" sx={styles?.mobileApp?.img} src={img} />
          ))}
        </Box>
      </Box>
    </CustomCard>
  );
}

export default MobileApp;
