import React from "react";
import HeadphonesOutlinedIcon from "@mui/icons-material/HeadphonesOutlined";
import { Typography, Box, Button } from "@mui/material";

import { CustomCard } from "../../../components/Reusable";
import { styles } from "./styles";

function Support() {
  return (
    <CustomCard lg={12}>
      <Box sx={styles.container}>
        <Typography variant="h5" sx={styles.invite.header}>
          <HeadphonesOutlinedIcon sx={{ fontSize: 30, mr: 1 }} />
          <span>Customer support</span>
        </Typography>
        <Box sx={styles.support.info}>
          <Box
            sx={styles.support.img}
            component="img"
            src="https://picsum.photos/300/300"
          />
          <Typography
            sx={{ fontWeight: "400", fontSize: "15px", lineHeight: "24px" }}
            variant="body1"
          >
            Simon is here to help you
          </Typography>
        </Box>

        <Button sx={{ mt: 2 }} variant="contained" color="secondary">
          Contact us
        </Button>
      </Box>
    </CustomCard>
  );
}

export default Support;
