import React from "react";
import { Box, Typography } from "@mui/material";
import BuildOutlinedIcon from "@mui/icons-material/BuildOutlined";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import { orange } from "@mui/material/colors";

import { CustomCard } from "../../../components/Reusable";
import { styles } from "./styles";

function Configure() {
  return (
    <CustomCard lg={12}>
      <Box sx={styles.container}>
        <Box sx={styles?.visitor?.topContainer}>
          <Typography sx={styles.configure.headtext}>
            <BuildOutlinedIcon sx={styles.headerIcon} />
            <span>Configure Your Shop</span>
          </Typography>
        </Box>
        <Box sx={styles?.info}>
          <Typography
            variant="h3"
            color="text.secondary.light"
            sx={styles.configure.infoText}
          >
            45%
          </Typography>
          <Typography variant="body1" sx={{ color: orange[800] }}>
            Completed
          </Typography>
        </Box>
        <Typography variant="body2" sx={styles.configure.infoPar}>
          Complete all the steps tohave a complete shop to best present to your
          customers.
        </Typography>
        <Typography sx={styles.link} color="text.secondary">
          <span>Complete the setup!</span> <ArrowForwardIcon sx={{ ml: 3.5 }} />
        </Typography>
      </Box>
    </CustomCard>
  );
}

export default Configure;
