import React from "react";
import { Box, Typography } from "@mui/material";
import DashboardCustomizeIcon from "@mui/icons-material/DashboardCustomize";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import Carousel from "react-elastic-carousel";

import { CustomCard } from "../../../components/Reusable";
import { styles } from "./styles";

function MarketPlace() {
  const breakpoints = [{ width: 1, itemsToShow: 2 }];
  return (
    <CustomCard>
      <Box sx={styles.container}>
        <Typography sx={styles.MarketPlace.headerText}>
          <DashboardCustomizeIcon sx={styles.MarketPlace.headerIcon} />
          <span>Extensions MarketPlace</span>
        </Typography>
        <Box
          style={{ marginLeft: "-5px", marginRight: "-20px" }}
          sx={styles.MarketPlace.info}
        >
          <Carousel
            style={{ width: "100%" }}
            pagination={false}
            itemsToScroll={1}
            enableAutoPlay={true}
            autoPlaySpeed={2000}
            showArrows={false}
            itemsToShow={1.7}
          >
            {options.map((data, i) => (
              <Card {...data} key={i} />
            ))}
          </Carousel>
        </Box>
        <Box>
          <Typography sx={styles.link} color="text.secondary">
            <span>Discover all extensions</span>{" "}
            <ArrowForwardIcon sx={{ ml: 3.5 }} />
          </Typography>
        </Box>
      </Box>
    </CustomCard>
  );
}

export default MarketPlace;

const Card = ({ Icon, text }) => (
  <Box>
    <Box sx={styles?.MarketPlace.iconContainer}>
      <Icon sx={{ color: "white", fontSize: "30px" }} />
    </Box>
    <Typography
      variant="h2"
      color="text.primary"
      sx={styles.MarketPlace.infoText}
    >
      {text}
    </Typography>
  </Box>
);

const options = [
  {
    text: "  Connect your Domain",
    Icon: ArrowForwardIcon,
  },
  {
    text: "  A new way of thinking",
    Icon: ArrowForwardIcon,
  },
  {
    text: "  Start something new",
    Icon: ArrowForwardIcon,
  },
  {
    text: "React development",
    Icon: ArrowForwardIcon,
  },
  {
    text: "Something amazing",
    Icon: ArrowForwardIcon,
  },
  {
    text: "  Connect your Domain",
    Icon: ArrowForwardIcon,
  },
  {
    text: "  A new way of thinking",
    Icon: ArrowForwardIcon,
  },
  {
    text: "  Start something new",
    Icon: ArrowForwardIcon,
  },
  {
    text: "React development",
    Icon: ArrowForwardIcon,
  },
  {
    text: "Something amazing",
    Icon: ArrowForwardIcon,
  },
  {
    text: "  Connect your Domain",
    Icon: ArrowForwardIcon,
  },
  {
    text: "  A new way of thinking",
    Icon: ArrowForwardIcon,
  },
  {
    text: "  Start something new",
    Icon: ArrowForwardIcon,
  },
  {
    text: "React development",
    Icon: ArrowForwardIcon,
  },
  {
    text: "Something amazing",
    Icon: ArrowForwardIcon,
  },
];
