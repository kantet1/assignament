import React from "react";
import { Box, Container, Grid } from "@mui/material";

import {
  Visitors,
  TrustPilot,
  Support,
  Orders,
  News,
  MobileApp,
  MarketPlace,
  Invite,
  Configure,
} from "./Cards";

//   import { CustomCard } from "../../../components/Reusable";
function DashContainer() {
  return (
    <Box
      sx={{
        minHeight: "auto",
        width: "100%",
      }}
    >
      <Grid
        sx={{ minHeight: "auto", width: "100%" }}
        container={true}
        spacing={2}
        columns={12}
      >
        <Grid item xs={12} lg={8} sx={{ width: "100%" }}>
          <Grid container sx={{ width: "100%" }}>
            <Visitors />
            <Orders />
            <MobileApp />
            <MarketPlace />
            <News />
          </Grid>
        </Grid>
        <Grid item xs={12} lg={4}>
          <Grid container sx={{ width: "100%" }}>
            <Configure />
            <TrustPilot />
            <Invite />
            <Support />
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
}
export default DashContainer;
