import React from "react";
import { Box, Typography, Container } from "@mui/material";
import LinkIcon from "@mui/icons-material/Link";

import { MainWrapper } from "../../wrapper";
import { styles } from "./styles";
import DashContainer from "./DashContainer";

function Dashboard() {
  return (
    <Box sx={styles?.dashContainer}>
      <Box sx={styles?.dashTop}>
        <Container sx={styles?.container}>
          <Typography sx={styles.greetings} variant="h1">
            Welcome Lincoln
          </Typography>
          <Box sx={styles?.dashLinks}>
            <Typography>app.vetrinalive.it/fenoh-store</Typography>
            <LinkIcon sx={{ ml: 2, mr: 2 }} />
          </Box>
        </Container>
      </Box>
      <Box sx={styles?.dashMainContainer}>
        <Container>
          <DashContainer />
        </Container>
      </Box>
    </Box>
  );
}

export default MainWrapper(Dashboard, "Dashboard");
