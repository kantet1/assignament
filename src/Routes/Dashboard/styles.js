import { blue } from "@mui/material/colors";

export const styles = {
  dashContainer: { minHeight: "100vh", width: "100%", position: "relative" },
  dashTop: {
    background: `linear-gradient(180deg, #21B8F9 0%, rgba(33, 184, 249, 0) 132.05%);`,
    height: {
      xs: "40vh",
      sm: "30vh",
      md: "20vh",
      lg: "40vh",
      color: "white",
    },
    width: "100%",
  },
  container: {
    display: "flex",
    alignItems: { xs: "flex-start", sm: "baseline" },
    flexDirection: { xs: "column", sm: "row" },
    justifyContent: { xs: "center", sm: "space-between" },
  },
  greetings: {
    fontSize: "34px",
    fontWeight: "600",
    lineHeight: "39px",
    pt: { xs: 5 },
    ml: 1,
    // fontFamily: "Source Sans Pro",
    fontStyle: "normal",
  },
  dashLinks: {
    fontWeight: "500",
    display: "flex",
    cursor: "pointer",
    textDecoration: "underline",
    mt: { xs: "10px", sm: 0 },
    fontSize: "17px",
    lineHeigh: "22px",
  },
  dashMainContainer: {
    width: "100%",
    position: "absolute",
    top: { xs: 180, sm: 160, lg: 120 },
  },
};
