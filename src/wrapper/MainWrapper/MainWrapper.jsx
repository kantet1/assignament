import React from "react";
import { Box } from "@mui/material";

import { NavBar, SideNav } from "../../components";

const MainWrapper = (Component, active) =>
  function HOC() {
    return (
      <Box sx={{ display: "flex" }}>
        <NavBar active={active} />
        <SideNav active={active} />
        <Component />
      </Box>
    );
  };

export default MainWrapper;
