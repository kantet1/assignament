import React from "react";
import { ThemeProvider, Box } from "@mui/material";
import CssBaseline from "@mui/material/CssBaseline";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import customTheme from "./theming";
import { Dashboard, Web, Customers } from "./Routes";
import helper from "./constants";

function App() {
  return (
    <ThemeProvider theme={customTheme}>
      <helper.DrawerHeader />
      <CssBaseline>
        <BrowserRouter>
          <Routes>
            <Route path="Dashboard" element={<Dashboard />} />
            <Route path="Customers" element={<Customers />} />
            <Route path="/" element={<Web />} />
          </Routes>
        </BrowserRouter>
      </CssBaseline>
    </ThemeProvider>
  );
}

export default App;
