import phone from "./phone.png";
import google from "./google.png";
import apple from "./apple.png";

export const images = { phone, google, apple };
