import { createTheme } from "@mui/material";
import { blue } from "@mui/material/colors";

const primaryColor = "#ffff";
const primaryText = "#333333";
const secondaryText = "#21B8F9";

const customTheme = createTheme({
  palette: {
    primary: {
      main: primaryColor,
    },
    secondary: {
      main: blue[500],
    },
    text: {
      primary: primaryText,
      secondary: secondaryText,
    },
  },

  typography: {
    fontFamily: "'Inter', sans-serif",
  },
});

export default customTheme;
